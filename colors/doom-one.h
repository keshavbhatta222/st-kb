static const char *colorname[] = {
    /* 8 normal colors */
    "#1c1f24", "#ff6c6b", "#98be65", "#da8548", "#51afef", "#c678dd", "#5699af",
    "#202328",

    /* 8 bright colors */
    "#5b6268", "#da8548", "#4db5bd", "#ecbe7b", "#3071db", "#a9a1e1", "#46d9ff",
    "#dfdfdf",

    [255] = 0,

    /* more colors can be added after 255 to use with DefaultXX */
    "#282c34", /* background */
    "#bbc2cf", /* foreground */
    "#51afef", /* cursor */
    "#51afef", /* background */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 12;
unsigned int defaultrcs = 12;
