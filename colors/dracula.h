static const char *colorname[] = {
    /* 8 normal colors */
    "#000000", "#ff5555", "#50fa7b", "#f1fa8c", "#bd93f9", "#ff79c6", "#8be9fd",
    "#bbbbbb",

    /* 8 bright colors */
    "#555555", "#ff5555", "#50fa7b", "#f1fa8c", "#caa9fa", "#ff79c6", "#8be9fd",
    "#ffffff",

    [255] = 0,

    /* more colors can be added after 255 to use with DefaultXX */
    "#181825", /* background */
    "#f8f8f2", /* foreground */
    "#caa9fa", /* cursor */
    "#181825", /* background */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
unsigned int defaultcs = 258;
unsigned int defaultrcs = 258;
