const char *colorname[] = {

    /* 8 normal colors */
    [0] = "#1e1e2e", /* black   */
    [1] = "#92216E", /* red     */
    [2] = "#87417E", /* green   */
    [3] = "#665B94", /* yellow  */
    [4] = "#A7328E", /* blue    */
    [5] = "#9F53A3", /* magenta */
    [6] = "#CC57C2", /* cyan    */
    [7] = "#acc2d2", /* white   */

    /* 8 bright colors */
    [8] = "#788793",  /* black   */
    [9] = "#92216E",  /* red     */
    [10] = "#87417E", /* green   */
    [11] = "#665B94", /* yellow  */
    [12] = "#A7328E", /* blue    */
    [13] = "#9F53A3", /* magenta */
    [14] = "#CC57C2", /* cyan    */
    [15] = "#acc2d2", /* white   */

    /* special colors */
    [256] = "#090610", /* background */
    [257] = "#acc2d2", /* foreground */
    [258] = "#51afef", /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs = 258;
